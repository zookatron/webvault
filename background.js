var vault_open = false;
var passphrase = null;
var sites = null;
var sites_id_map = null;
var prefs = null;
var cache = null;
var MAX_SITES_PER_CHUNK = 50;

var autofill_menu = null;

tabsOnActivatedAddListener(function(activeInfo) {
	if(autofill_menu) {
		contextMenusRemove(autofill_menu);
		autofill_menu = null;
	}
});

runtimeOnMessageAddListener(function(request, sender, respond) {
	if('action' in request) {
		if(request.action == 'get_vault_state') {
			storageSyncGet('version').then(function(items) {
				var state = 'none';
				if(items && 'version' in items) {
					state = vault_open ? 'open' : 'closed';
				}
				respond({state: state});
			}, function(error) {
				respond({error: error});
			});
			return true;
		} else if(request.action == 'open_vault') {
			if(typeof request.passphrase == 'string' && request.passphrase != '') {
				storageSyncGet('passphrase_hash').then(function(items) {
					if('passphrase_hash' in items) {
						if(hash_passphrase(request.passphrase) === items.passphrase_hash) {
							passphrase = request.passphrase;
							load_vault(respond);
						} else {
							respond({error: 'Incorrect passphrase'});
						}
					} else {
						create_vault(request.passphrase, respond);
					}
				});
				return true;
			} else {
				respond({error: 'Please enter a passphrase'});
			}
		} else if(request.action == 'close_vault') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			close_vault();
			respond({});
		} else if(request.action == 'get_sites') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			reset_lock_timer();
			respond({sites: sites});
		} else if(request.action == 'get_prefs') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			var data = jQuery.extend({}, prefs, { id: 0, remotestorage_address: cache.credentials ? cache.credentials.address : '' });

			reset_lock_timer();
			respond({prefs: data});
		} else if(request.action == 'update_prefs') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			prefs = { locktime: request.prefs.locktime };

			save_vault(respond);
			return true;
		} else if(request.action == 'export') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			var data = export_data();

			reset_lock_timer();
			respond(data);
		} else if(request.action == 'import') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			create_vault(passphrase, respond, update_data_format(request.data));
			return true;
		} else if(request.action == 'remotestorage_import') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			get_remotestorage_credentials(request.address).then(function(credentials) {
				return get_remotestorage_data(credentials, 'webvault/data').then(function(data) {
					if(!data) {
						respond({ error: 'No saved WebVault data found!' }); return;
					}
					try {
						data = decrypt(data, passphrase);
					} catch(error) {
						respond({ error: 'Unable to decrypt WebVault data (usually due to incorrect passphrase)' }); return;
					}
					create_vault(passphrase, respond, update_data_format(data));
				});
			}).catch(function(error) { respond({ error: error.message }) });

			reset_lock_timer();
			return true;
		} else if(request.action == 'remotestorage_export') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			get_remotestorage_credentials(request.address).then(function(credentials) {
				return put_remotestorage_data(credentials, 'webvault/data', encrypt(export_data(), passphrase));
			}).then(function() { respond({}); }, function(error) { respond({ error: error.message }); });

			reset_lock_timer();
			return true;
		} else if(request.action == 'erase_data') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			close_vault();
			storageSyncRemove(['version', 'passphrase_hash', 'sites', 'prefs', 'cache']);

			respond({});
		} else if(request.action == 'create_site') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			var site = request.site;
			var newid = sites.length > 0 ? sites[sites.length-1].id+1 : 1;
			site.id = newid;
			sites.push(site);
			sites_id_map[newid] = site;

			save_vault(function(response) {
				if('error' in response) {
					erase_site(newid);
					respond(response);
				} else {
					respond({id: newid});
				}
			});
			return true;
		} else if(request.action == 'update_site') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			for(attribute in request.site) {
				sites_id_map[request.site.id][attribute] = request.site[attribute];
			}

			save_vault(respond);
			return true;
		} else if(request.action == 'delete_site') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }

			erase_site(request.site_id);

			save_vault(respond);
			return true;
		} else if(request.action == 'gen_password') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }
			reset_lock_timer();
			respond(gen_password(request.data));
		} else if(request.action == 'reset_context_menu') {
			if(autofill_menu) {
				contextMenusRemove(autofill_menu);
				autofill_menu = null;
			}
		} else if(request.action == 'set_context_menu') {
			if(autofill_menu) {
				contextMenusRemove(autofill_menu);
				autofill_menu = null;
			}

			autofill_menu = contextMenusCreate({
				title: 'Autofill',
				contexts: ['editable'],
			});

			if(!vault_open) {
				contextMenusCreate({
					title: 'Vault is locked',
					contexts: ['editable'],
					parentId: autofill_menu,
				});
			} else {
				var service_url = parse_url(sender.tab.url).hostname.replace('www.', '');
				var autofill_sites = [];
				for(i in sites) {
					if(sites[i].service_url === service_url) { autofill_sites.push(sites[i]); }
				}

				if(autofill_sites.length > 0) {
					for(var i = autofill_sites.length - 1; i >= 0; i--) {
						contextMenusCreate({
							title: autofill_sites[i].service_name,
							contexts: ['editable'],
							parentId: autofill_menu,
							onclick: function(sender_id, site_id, form_id, info, tab) {
								tabsSendMessage(sender_id, {
									action: 'do_autofill',
									site_id: site_id,
									form_id: form_id
								});
							}.bind(this, sender.tab.id, autofill_sites[i].id, request.form_id)
						});
					}
				} else {
					contextMenusCreate({
						title: 'No saved passwords for this site',
						contexts: ['editable'],
						parentId: autofill_menu,
					});
				}
			}
		} else if(request.action == 'query_automatic_fill') {
			if(!vault_open) { return; }

			var current_site = parse_url(sender.tab.url).hostname;
			var autofill_site = null;
			for(i in sites) {
				var site = sites[i];
				if(current_site.indexOf(site.service_url) !== -1 && site.pageload_autofill) {
					autofill_site = site;
					break;
				}
			}

			if(autofill_site) {
				tabsSendMessage(sender.tab.id, {
					action: 'do_autofill',
					site_id: autofill_site.id,
					form_id: request.form_id,
				});
			}
		} else if(request.action == 'request_autofill') {
			if(!vault_open) { respond({error: 'vault is not open'}); return; }
			if(!(request.site_id in sites_id_map)) { respond({error: 'site not found'}); return; }

			var site = sites_id_map[request.site_id];

			var form_id = request.form_id;
			if(form_id < 0) {
				if('last_form_id' in site) {
					form_id = site.last_form_id;
				}
			} else {
				site.last_form_id = request.form_id;
			}
			if(!site.importance) { site.importance = 0; }
			site.importance += 1;
			save_vault();

			reset_lock_timer();
			var password = gen_password(site);
			if(password.error) { respond({error: password.error}); return; }
			respond({
				form_id: form_id,
				username: site.username,
				password: password.value,
			});
		}
	}
});

function create_vault(pass, callback, defaults) {
	defaults = typeof defaults !== 'undefined' ? defaults : get_defaults();
	vault_open = true;
	passphrase = pass;
	sites = defaults.sites;
	prefs = defaults.prefs;
	cache = cache ? cache : get_defaults().cache;
	build_sites_id_map();
	save_vault(callback);
}

function load_vault(callback) {
	var finish = function(value) { reset_lock_timer(); if(callback) { callback(value); } }

	maybe_update_storage(function(value) {
		if(value.error) { return finish(value); }
		storage_read(function(items) {
			if(items.error) { return finish(items); }
			try {
				sites = items.sites;
				prefs = items.prefs;
				cache = items.cache;
				build_sites_id_map();
				vault_open = true;
				finish({});
			} catch(error) {
				finish({error: error.message});
			}
		});
	});
}

function save_vault(callback) {
	if(!vault_open) { return; }

	var finish = function(value) { reset_lock_timer(); if(callback) { callback(value); } }

	storage_write({ sites: sites, prefs: prefs, cache: cache }, finish);
}

function maybe_update_storage(callback) {
	var finish = function(value) { if(callback) { callback(value); } }

	storageSyncGet(['version']).then(function(version) {
		if(!version.version) {
			finish({error: 'no data found when attempting to load vault'});
		} else if(version_compare(version.version, '0.5.0') < 0) {
			finish({error: 'incompatable version '+version.version});
		} else if(version_compare(version.version, '0.5.3') < 0) {
			storageSyncSet({cache: encrypt(get_defaults().cache, passphrase)}).then(function() {
				finish({});
			}, function(error) {
				finish({error: error.message});
			});
		} else {
			finish({});
		}
	});
}

function storage_read(callback) {
	storageSyncGet(['sites_parts', 'prefs', 'cache']).then(function(items) {
		var result = {};
		result.prefs = decrypt(items.prefs, passphrase);
		result.cache = decrypt(items.cache, passphrase);
		result.sites = [];

		return storageSyncGet(items.sites_parts).then(function(sites_parts) {
			for(var part_name in sites_parts) {
				result.sites = result.sites.concat(uncompress_sites(decrypt(sites_parts[part_name], passphrase)));
			}
			return result;
		});
	}).then(function(result) {
		callback(result);
	}, function(error) {
		callback({ error: error.message });
	});
}

function storage_write(data, callback) {
	var finish = function(value) { if(callback) { callback(value); } }

	try {
		parsed_data = {};
		parsed_data.passphrase_hash = hash_passphrase(passphrase);
		parsed_data.prefs = encrypt(data.prefs, passphrase);
		parsed_data.cache = encrypt(data.cache, passphrase);
		parsed_data.version = runtimeGetManifest().version;
		parsed_data.sites_parts = [];

		var sites_parts = chunk(compress_sites(data.sites), MAX_SITES_PER_CHUNK).map(function(part) { return encrypt(part, passphrase); });
		for(let i = 0; i < sites_parts.length; i++) {
			var name = 'sites_part_'+i.toString();
			parsed_data[name] = sites_parts[i];
			parsed_data.sites_parts.push(name);
		}
	} catch(error) {
		return finish({error: error.message});
	}

	storageSyncSet(parsed_data).then(function() {
		finish({});
	}, function(error) {
		finish({error: error.message});
	});
}

function close_vault() {
	vault_open = false;
	passphrase = null;
	sites = null;
	sites_id_map = null;
	prefs = null;
	cache = null;
}

function erase_site(site_id) {
	for(var i = sites.length - 1; i >= 0; i--) {
		if(sites[i].id === site_id) {
			sites.splice(i, 1);
		}
	}
	delete sites_id_map[site_id];
}

function build_sites_id_map() {
	sites_id_map = {};
	for(var i = sites.length - 1; i >= 0; i--) {
		sites_id_map[sites[i].id] = sites[i];
	}
}

function get_defaults() {
	return {
		sites: [],
		prefs: {locktime: 0},
		cache: {},
	};
}

var site_keys = ['id', 'service_name', 'service_url', 'username',
'pass_length', 'pass_repetition', 'pass_requiredlen', 'pass_lower',
'pass_upper', 'pass_number', 'pass_dash', 'pass_space', 'pass_symbol',
'pageload_autofill', 'last_form_id', 'importance'];
var pass_options = {
	pass_lower: 'lower',
	pass_upper: 'upper',
	pass_number: 'number',
	pass_dash: 'dash',
	pass_space: 'space',
	pass_symbol: 'symbol',
};
var compress_pass_options_values = {
	allowed: 0,
	required: 1,
	forbidden: 2,
};
var uncompress_pass_options_values = {
	0: 'allowed',
	1: 'required',
	2: 'forbidden',
};

function compress_sites(site_list) {
	var compressed_sites = [];
	for(i in site_list) {
		var normalized_site = jQuery.extend({}, site_list[i]);

		if(typeof normalized_site['id'] == 'string') { normalized_site['id'] = parseInt(normalized_site['id']); }
		if(typeof normalized_site['pass_length'] == 'string') { normalized_site['pass_length'] = parseInt(normalized_site['pass_length']); }
		if(typeof normalized_site['pass_repetition'] == 'string') { normalized_site['pass_repetition'] = parseInt(normalized_site['pass_repetition']); }
		if(typeof normalized_site['pass_requiredlen'] == 'string') { normalized_site['pass_requiredlen'] = parseInt(normalized_site['pass_requiredlen']); }
		if(!('last_form_id' in normalized_site)) { normalized_site['last_form_id'] = -1; }
		if(typeof normalized_site['last_form_id'] == 'string') { normalized_site['last_form_id'] = parseInt(normalized_site['last_form_id']); }
		if(!('importance' in normalized_site)) { normalized_site['importance'] = 0; }

		for(option in pass_options) {
			normalized_site[option] = compress_pass_options_values[normalized_site[option]];
		}

		var compressed_site = [];
		for(var j = 0; j < site_keys.length; j++) {
			compressed_site.push(normalized_site[site_keys[j]]);
		}
		compressed_sites.push(compressed_site);
	}
	return compressed_sites;
}

function uncompress_sites(site_list) {
	var uncompressed_sites = [];
	for(i in site_list) {
		var uncompressed_site = {};

		for(var j = 0; j < site_keys.length; j++) {
			uncompressed_site[site_keys[j]] = site_list[i][j];
		};

		for(option in pass_options) {
			uncompressed_site[option] = uncompress_pass_options_values[uncompressed_site[option]];
		}

		uncompressed_sites.push(uncompressed_site);
	}
	return uncompressed_sites;
}

var lock_timer = null;
function reset_lock_timer() {
	if(!vault_open) { return; }
	if(lock_timer) { clearTimeout(lock_timer); }
	if(prefs.locktime <= 0) { return; }
	lock_timer = setTimeout(function() {
		close_vault();
		runtimeSendMessage({action: 'vault_state_change'});
	}, prefs.locktime*60*1000);
}

function gen_password(data) {
	if(!data.service_name) { return { error: 'no service name' }; }

	var settings = {
		length: data.pass_length,
		repeat: data.pass_repetition,
		phrase: passphrase,
	}
	var rlength = parseInt(data.pass_requiredlen);
	for(option in pass_options) {
		var option_value = data[option];
		if(option_value === 'forbidden') {
			settings[pass_options[option]] = 0;
		} else if(option_value === 'required') {
			settings[pass_options[option]] = rlength;
		}
	}

	try {
		return { value: new Vault(settings).generate(data.service_name) };
	} catch(error) {
		return { error: error.message };
	}
}

function hash_passphrase(pass) {
	return new Vault({phrase:pass}).generate('webvault');
}

function encrypt(data, pass) {
	var bytes = msgpack.encode(data);
	var payload = String.fromCharCode.apply(null, bytes);
	var encrypted = CryptoJS.AES.encrypt(payload, pass).toString();
	return encrypted;
}

function decrypt(data, pass) {
	var decrypted = CryptoJS.AES.decrypt(data, pass).toString(CryptoJS.enc.Utf8);
	var bytes = new Uint8Array(decrypted.length);
	for(var i = 0; i < decrypted.length; i++) {
		bytes[i] = decrypted.charCodeAt(i);
	}
	let result = msgpack.decode(bytes);
	return result;
}

function export_data() {
	var data = {
		sites: sites,
		prefs: prefs,
		version: runtimeGetManifest().version,
	};

	return data;
}

function update_data_format(data) {
	if(!data || !data.version) {
		return get_defaults();
	}
	if(version_compare(data.version, '0.3.0') < 0) {
		var sitelist = [];
		for(id in data.sites) {
			sitelist.push(data.sites[id]);
		}
		data.sites = sitelist;
	}
	if(version_compare(data.version, '0.4.0') < 0) {
		for(i in data.sites) {
			if(!data.sites[i].importance) { data.sites[i].importance = 0; }
		}
	}
	return data;
}

function get_remotestorage_credentials(address) {
	var address_parts = address.split('@');
	if(address_parts.length !== 2) { return Promise.reject(new Error('Invalid RemoteStorage address provided!')); }
	var address_host = address_parts[1];

	var get_new_credentials = function() {
		return new Promise(function(resolve, reject) {
			jQuery.ajax('https://'+address_host+'/.well-known/webfinger?resource='+encodeURIComponent('acct:'+address)).done(function(response) {
				var remotestorage = find(response.links, { rel: 'http://tools.ietf.org/id/draft-dejong-remotestorage' });
				if(!remotestorage) { reject(new Error('No RemoteStorage information found for account "'+address+'"!')); return; }
				var remotestorage_url = remotestorage.href;
				var oauth_host = remotestorage.properties['http://tools.ietf.org/html/rfc6749#section-4.2'];
				if(!oauth_host) { reject(new Error('No RemoteStorage OAuth URL found for account "'+address+'"!')); return; }
				var redirect_uri = identityGetRedirectURL();
				var oauth_url = [
					oauth_host,
					'?redirect_uri='+encodeURIComponent(redirect_uri),
					'&scope='+encodeURIComponent('webvault:rw'),
					'&client_id='+encodeURIComponent(redirect_uri),
					'&response_type=token',
				].join('');

				identityLaunchWebAuthFlow({ interactive: true, url: oauth_url }).then(function(responseURL) {
					var response_parts = responseURL.split('#access_token=');
					if(response_parts.length !== 2) { reject(new Error('Invalid access token response from "'+address_host+'"!')); return; }
					var access_token = response_parts[1];
					var credentials = { address: address, url: remotestorage_url, bearer_token: access_token };
					cache.credentials = credentials;
					save_vault(function() { resolve(credentials); });
				}, function(error) {
					reject(new Error('Unable to aquire RemoteStorage OAuth credentials: '+error.message));
				});
			}).fail(function() {
				reject(new Error('Unable to connect to "'+address_host+'"!'));
			});
		});
	};

	if(cache.credentials && cache.credentials.address === address) {
		return get_remotestorage_data(cache.credentials, 'webvault/data').then(function(data) {
			return cache.credentials;
		}, function(error) {
			return get_new_credentials();
		});
	} else {
		return get_new_credentials();
	}
}

function put_remotestorage_data(credentials, path, data) {
	return new Promise(function(resolve, reject) {
		jQuery.ajax(credentials.url+'/'+path, {
			method: 'PUT',
			headers: {
				'Authorization': 'Bearer '+credentials.bearer_token,
				'Content-Type': 'text/plain',
			},
			data: data,
		}).done(function(response) {
			resolve();
		}).fail(function(error) {
			reject(new Error(error && error.responseText ? 'Error from "'+credentials.url+'": '+error.responseText : 'Unable to connect to "'+credentials.url+'"!'));
		});
	});
}

function get_remotestorage_data(credentials, path) {
	return new Promise(function(resolve, reject) {
		jQuery.ajax(credentials.url+'/'+path, {
			method: 'GET',
			headers: { 'Authorization': 'Bearer '+credentials.bearer_token },
		}).done(function(response) {
			resolve(response);
		}).fail(function(error) {
			if(error && error.status === 404) {
				resolve(null);
			} else {
				reject(new Error(error && error.responseText ? 'Error from "'+credentials.url+'": '+error.responseText : 'Unable to connect to "'+credentials.url+'"!'));
			}
		});
	});
}

/* Utility Functions */

function parse_url(url) {
	if(url.search('//') === -1) { url = '//'+url; }
	// url parsing trick from https://gist.github.com/jlong/2428561
	var parser = document.createElement('a');
	parser.href = url;
	return parser;
}

function version_compare(v1, v2) {
	v1parts = v1.split('.');
	v2parts = v2.split('.');

	v1parts = v1parts.map(Number);
	v2parts = v2parts.map(Number);

	for(var i = 0; i < v1parts.length; ++i) {
		if(v2parts.length == i) { return 1; }

		if(v1parts[i] == v2parts[i]) { continue; }
		else if(v1parts[i] > v2parts[i]) { return 1; }
		else { return -1; }
	}

	if(v1parts.length != v2parts.length) { return -1; }

	return 0;
}

function chunk(array, size) {
	size = Math.max(size, 0);
	var length = array == null ? 0 : array.length;
	if(!length || size < 1) { return []; }
	var index = 0;
	var resIndex = 0;
	var result = new Array(Math.ceil(length / size));
	while(index < length) {
		result[resIndex++] = array.slice(index, (index += size));
	}
	return result;
}

function find(array, props) {
	for(var i = 0; i < array.length; i++) {
		var hasAll = true;
		for(var prop in props) {
			if(!array[i] || !array[i][prop] === props[prop]) {
				hasAll = false;
				break;
			}
		}
		if(hasAll) { return array[i]; }
	}
	return null;
}

// Cross browser functions

function tabsOnActivatedAddListener(listener) {
	if(window.chrome) {
		return chrome.tabs.onActivated.addListener(listener);
	} else {
		return browser.tabs.onActivated.addListener(listener);
	}
}

function runtimeOnMessageAddListener(listener) {
	if(window.chrome) {
		return chrome.runtime.onMessage.addListener(listener);
	} else {
		return browser.runtime.onMessage.addListener(listener);
	}
}

function runtimeSendMessage(message) {
	if(window.chrome) {
		return new Promise(function(resolve, reject) {
			chrome.runtime.sendMessage(message, function(result) {
				if(chrome.runtime.lastError) {
					reject(chrome.runtime.lastError);
				} else {
					resolve(result);
				}
			});
		});
	} else {
		return browser.runtime.sendMessage(message);
	}
}

function runtimeGetManifest() {
	if(window.chrome) {
		return chrome.runtime.getManifest();
	} else {
		return browser.runtime.getManifest();
	}
}

function storageSyncGet(keys) {
	if(window.chrome) {
		return new Promise(function(resolve, reject) {
			chrome.storage.sync.get(keys, function(result) {
				if(chrome.runtime.lastError) {
					reject(chrome.runtime.lastError);
				} else {
					resolve(result);
				}
			});
		});
	} else {
		return browser.storage.sync.get(keys);
	}
}

function storageSyncSet(keys) {
	if(window.chrome) {
		return new Promise(function(resolve, reject) {
			chrome.storage.sync.set(keys, function(result) {
				if(chrome.runtime.lastError) {
					reject(chrome.runtime.lastError);
				} else {
					resolve(result);
				}
			});
		});
	} else {
		return browser.storage.sync.set(keys);
	}
}

function storageSyncRemove(keys) {
	if(window.chrome) {
		return new Promise(function(resolve, reject) {
			chrome.storage.sync.remove(keys, function(result) {
				if(chrome.runtime.lastError) {
					reject(chrome.runtime.lastError);
				} else {
					resolve(result);
				}
			});
		});
	} else {
		return browser.storage.sync.remove(keys);
	}
}

function contextMenusCreate(options, callback) {
	if(window.chrome) {
		return chrome.contextMenus.create(options, callback);
	} else {
		return browser.menus.create(options, callback);
	}
}

function contextMenusRemove(menuId) {
	if(window.chrome) {
		return new Promise(function(resolve, reject) {
			chrome.contextMenus.remove(menuId, function(result) {
				if(chrome.runtime.lastError) {
					reject(chrome.runtime.lastError);
				} else {
					resolve(result);
				}
			});
		});
	} else {
		return browser.menus.remove(menuId);
	}
}

function tabsSendMessage(tabId, message) {
	if(window.chrome) {
		return new Promise(function(resolve, reject) {
			chrome.tabs.sendMessage(tabId, message, function(result) {
				if(chrome.runtime.lastError) {
					reject(chrome.runtime.lastError);
				} else {
					resolve(result);
				}
			});
		});
	} else {
		return browser.tabs.sendMessage(tabId, message);
	}
}

function identityGetRedirectURL() {
	if(window.chrome) {
		return chrome.identity.getRedirectURL();
	} else {
		return browser.identity.getRedirectURL();
	}
}

function identityLaunchWebAuthFlow(options) {
	if(window.chrome) {
		return new Promise(function(resolve, reject) {
			chrome.identity.launchWebAuthFlow(options, function(result) {
				if(chrome.runtime.lastError) {
					reject(chrome.runtime.lastError);
				} else {
					resolve(result);
				}
			});
		});
	} else {
		return browser.identity.launchWebAuthFlow(options);
	}
}
