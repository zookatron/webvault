jQuery(document).ready(function() {

	runtimeOnMessageAddListener(function(request, sender, respond) {
		if('action' in request && request.action == 'vault_state_change') {
			container.get_vault_state();
		}
	});

	var Preferences = Backbone.Model.extend({
		defaults: function() {
			return {
				id: 0,
				locktime: 0,
			};
		},
		sync: function(method, model, options) {
			switch(method) {
				case 'read':
					runtimeSendMessage({action: 'get_prefs'}).then(function(response) {
						if('error' in response) {
							options.error(response.error);
						} else {
							options.success(response.prefs);
						}
					});
					break;
				case 'update':
					runtimeSendMessage({action: 'update_prefs', prefs: model.attributes}).then(function(response) {
						if('error' in response) {
							options.error(response.error);
						} else {
							options.success();
						}
					});
					break;
				default:
					options.error();
					break;
			}
		}
	});
	var prefs = new Preferences();

	var Site = Backbone.Model.extend({
		defaults: function() {
			return {
				service_name: "",
				service_url: "",
				username: "",
				pass_length: 20,
				pass_repetition: 0,
				pass_requiredlen: 2,
				pass_lower: "allowed",
				pass_upper: "allowed",
				pass_number: "allowed",
				pass_dash: "allowed",
				pass_space: "allowed",
				pass_symbol: "allowed",
				pageload_autofill: false,
				importance: 0,
			};
		},
		sync: function(method, model, options) {
			switch(method) {
				case 'create':
					runtimeSendMessage({action: 'create_site', site: model.attributes}).then(function(response) {
						if('error' in response) {
							options.error(response.error);
						} else {
							model.set('id', response.id);
							options.success();
						}
					});
					break;
				case 'update':
					runtimeSendMessage({action: 'update_site', site: model.attributes}).then(function(response) {
						if('error' in response) {
							options.error(response.error);
						} else {
							options.success();
						}
					});
					break;
				case 'delete':
					runtimeSendMessage({action: 'delete_site', site_id: model.id}).then(function(response) {
						if('error' in response) {
							options.error(response.error);
						} else {
							options.success();
						}
					});
					break;
				default:
					options.error();
					break;
			}
		}
	});

	var SiteList = Backbone.Collection.extend({
		model: Site,
		comparator: function(site) { return -site.get('importance'); },
		sync: function(method, model, options) {
			switch(method) {
				case 'read':
					runtimeSendMessage({action: 'get_sites'}).then(function(response) {
						if('error' in response) {
							options.error(response.error);
						} else {
							options.success(response.sites);
						}
					});
					break;
				default:
					options.error();
					break;
			}
		}
	});
	var sites = new SiteList();

	var EditSiteView = Backbone.View.extend({
		template: createTemplate(jQuery('#editsite_template').html()),
		events: {
			'click .header .back': 'back',
			'click .save-button': 'save',
			'keyup input' : 'update_password',
			'change input' : 'update_password',
			'change #service-url' : 'format_url',
		},
		initialize: function(options) {
			if('site' in options) {
				this.model = options.site;
			} else {
				this.model = new Site();

				var self = this;
				tabsQuery({currentWindow: true, active: true}).then(function(tabs) {
					if(tabs.length < 1) { return; }
					self.model.set({service_url: parse_url(tabs[0].url).hostname.split('.').slice(-2).join('.')});
					self.render();
				});
			}
		},
		render: function() {
			this.$el.html(this.template({}));

			this.$('#service-name').val(this.model.get('service_name'));
			this.$('#service-url').val(this.model.get('service_url'));
			this.$('#username').val(this.model.get('username'));
			this.$('#gen-length').val(this.model.get('pass_length'));
			this.$('#repetition').val(this.model.get('pass_repetition'));
			this.$('#requiredlen').val(this.model.get('pass_requiredlen'));

			this.$('input:radio[name=lower]').filter('[value='+this.model.get('pass_lower')+']').prop('checked','checked');
			this.$('input:radio[name=upper]').filter('[value='+this.model.get('pass_upper')+']').prop('checked','checked');
			this.$('input:radio[name=number]').filter('[value='+this.model.get('pass_number')+']').prop('checked','checked');
			this.$('input:radio[name=dash]').filter('[value='+this.model.get('pass_dash')+']').prop('checked','checked');
			this.$('input:radio[name=space]').filter('[value='+this.model.get('pass_space')+']').prop('checked','checked');
			this.$('input:radio[name=symbol]').filter('[value='+this.model.get('pass_symbol')+']').prop('checked','checked');

			if(this.model.get('pageload_autofill')) { this.$('#pageload-autofill').prop('checked','checked'); }

			this.update_password();

			return this;
		},
		back: function() {
			container.change_view('mainmenu');
		},
		format_url: function() {
			this.$('#service-url').val(parse_url(this.$('#service-url').val()).hostname);
		},
		get_data: function() {
			return {
				service_name: this.$('#service-name').val(),
				service_url: this.$('#service-url').val(),
				username: this.$('#username').val(),
				pass_length: this.$('#gen-length').val(),
				pass_repetition: this.$('#repetition').val(),
				pass_requiredlen: this.$('#requiredlen').val(),
				pass_lower: this.$('input:radio[name=lower]:checked').val(),
				pass_upper: this.$('input:radio[name=upper]:checked').val(),
				pass_number: this.$('input:radio[name=number]:checked').val(),
				pass_dash: this.$('input:radio[name=dash]:checked').val(),
				pass_space: this.$('input:radio[name=space]:checked').val(),
				pass_symbol: this.$('input:radio[name=symbol]:checked').val(),
				pageload_autofill: this.$('#pageload-autofill').is(':checked'),
			};
		},
		save: function() {
			if(!this.$('#service-name').val()) {
				this.$('#service-name').addClass('error');
				return;
			}

			if(this.$('.save-button').hasClass('disabled')) { return; }
			this.$('.save-button').addClass('disabled');

			this.model.save(this.get_data(), {success: function(model) {
				sites.add(model);
				container.change_view('mainmenu');
			}, error: function(model, response) {
				this.$('.save-button').removeClass('disabled');
				self.$('.errors').text(response).show();
			}});
		},
		update_password: function() {
			var self = this;
			runtimeSendMessage({action: 'gen_password', data: this.get_data()}).then(function(response) {
				if(response.error) {
					self.$('.errors').text(response.error).show();
					self.$('.generator .password').text('');
				} else {
					self.$('.errors').hide();
					self.$('.generator .password').text(response.value);
				}
			});
		}
	});

	var SiteView = Backbone.View.extend({
		tagName: 'li',
		template_normal: createTemplate(jQuery('#site_template').html()),
		template_confirm_delete: createTemplate(jQuery('#site_confirm_delete_template').html()),
		events: {
			'click .menubutton': 'clicked',
			'mouseover .menubutton': 'hovered',
		},
		initialize: function() {
			this.state = 'normal';
		},
		render: function() {
			this.$el.html(this['template_'+this.state]({name: this.model.get('service_name')}));
			return this;
		},
		clicked: function(event) {
			if(jQuery(event.target).hasClass('copy')) {
				copy_to_clipboard(this.password);
			} else if(jQuery(event.target).hasClass('edit')) {
				container.change_view('editsite', {site: this.model});
			} else if(jQuery(event.target).hasClass('delete')) {
				this.state = 'confirm_delete';
				this.render();
			} else if(jQuery(event.target).hasClass('confirm_delete')) {
				this.model.destroy();
			} else if(jQuery(event.target).hasClass('cancel_delete')) {
				this.state = 'normal';
				this.render();
			} else if(this.state == 'normal') {
				this.trigger('click', this);
			}
		},
		hovered: function() {
			if(!this.password) {
				var self = this;
				runtimeSendMessage({action: 'gen_password', data: this.model.toJSON()}).then(function(response) {
					if(response.value) { self.password = response.value; }
				});
			}
		},
	});

	var SiteListView = Backbone.View.extend({
		template: createTemplate(jQuery('#sitelist_template').html()),
		initialize: function() {
			this.listenTo(sites, 'destroy', this.render);
			this.listenTo(sites, 'reset', this.render);
			this.search = '';
		},
		render: function() {
			this.$el.html(this.template({}));
			this.render_sitelist();
			return this;
		},
		render_sitelist: function() {
			this.$('.submenu').empty();

			var models = sites.models;
			if(this.search != '') {
				models = new Fuse(sites.models, {
					keys: ['service_name'],
					getFn: function(obj, path) { return obj.get(path); }
				}).search(this.search);
			}

			for(let index = 0; index < models.length; index++) {
				var model = models[index];
				var newview = new SiteView({model: model});
				this.$('.submenu').append(newview.render().el);
				newview.on('click', this.site_clicked);
			}
		},
		site_clicked: function(site) {
			container.change_view('editsite', {site: site.model});
		},
		set_search: function(search) {
			this.search = search;
			this.render_sitelist();
		}
	});

	var AutofillView = Backbone.View.extend({
		template: createTemplate(jQuery('#autofill_template').html()),
		initialize: function() {
			this.listenTo(sites, 'destroy', this.render);
			this.listenTo(sites, 'reset', this.render);
			this.current_site = '';

			var self = this;
			tabsQuery({currentWindow: true, active: true}).then(function(tabs) {
				if(tabs.length < 1) { return; }
				self.current_site = parse_url(tabs[0].url).hostname;
				self.render();
			});
		},
		render: function() {
			this.$el.html(this.template({}));

			var viewsites = [];
            sites.each(function(site) {
				if(this.current_site.indexOf(site.get('service_url')) !== -1) {
					viewsites.push(site);
				}
            }, this);

			for(let index = 0; index < viewsites.length; index++) {
				var model = viewsites[index];
				var newview = new SiteView({model: model});
				this.$('.submenu').append(newview.render().el);
				newview.on('click', this.site_clicked);
			}

			if(viewsites.length > 0) {
				this.$el.removeClass('disabled');
				this.$el.addClass('expanded');
			} else {
				this.$el.addClass('disabled');
			}

			return this;
		},
		site_clicked: function(site) {
			tabsQuery({currentWindow: true, active: true}).then(function(tabs) {
				if(tabs.length < 1) { return; }
				tabsSendMessage(tabs[0].id, {
					action: 'do_autofill',
					site_id: site.model.id,
					form_id: -1,
				});
				window.close();
			});
		}
	});

	var MainMenuView = Backbone.View.extend({
		template: createTemplate(jQuery('#mainmenu_template').html()),
		events: {
			'click #preferences .menubutton': 'open_preferences',
			'click #lock-vault .menubutton': 'close_vault',
			'click #sites > .menubutton': 'toggle_sites',
			'input #sites #search': 'search_sites',
			'click #sites .add-site': 'add_site',
			'click #autofill:not(.disabled) > .menubutton': 'toggle_autofill',
		},
		initialize: function() {
			this.sites_view = new SiteListView();
			this.autofill_view = new AutofillView();
			this.rendered = false;
		},
		render: function() {
			this.$el.html(this.template({}));
			this.sites_view.setElement(this.$('#sites')).render();
			this.autofill_view.setElement(this.$('#autofill')).render();
			//This has to happen after rendering instead of in initialize() because of a race condition.
			//If the fetch returns while the initial rendering is still going on, both render calls will
			//be executing concurrently and will step on each others toes with DOM manipulations.
			if(!this.rendered) { this.rendered = true; sites.fetch({reset: true}); }
			return this;
		},
		open_preferences: function() {
			container.change_view('preferences');
		},
		close_vault: function() {
			runtimeSendMessage({action:'close_vault'}).then(function(response) {
				container.change_view('openvault');
			});
		},
		toggle_sites: function() {
			this.$('#sites').toggleClass('expanded');
		},
		toggle_autofill: function() {
			this.$('#autofill').toggleClass('expanded');
		},
		search_sites: function() {
			this.sites_view.set_search(this.$('#sites #search').val());
		},
		add_site: function() {
			container.change_view('editsite');
		}
	});

	var OpenVaultView = Backbone.View.extend({
		events: {
			'submit #passphrase-form': 'open_vault',
			'change #show-passphrase': 'update_password',
			'click .help a': 'open_help',
		},
		initialize: function(options) {
			if('vault_created' in options && !options.vault_created) {
				this.template = createTemplate(jQuery('#newvault_template').html());
			} else {
				this.template = createTemplate(jQuery('#openvault_template').html());
			}
		},
		render: function() {
			this.$el.html(this.template({}));
			this.update_password();
			return this;
		},
		open_help: function(e) {
 			tabsCreate({url: e.target.href});
			return false;
		},
		open_vault: function() {
			var self = this;
			runtimeSendMessage({
				action: 'open_vault',
				passphrase: this.$('#passphrase').val(),
			}).then(function(response) {
				if('error' in response) {
					self.$('.errors').text(response.error).show();
				} else {
					container.change_view('mainmenu');
				}
			});
			return false;
		},
		update_password: function() {
			var phrase = this.$('#passphrase');
			var checkbox = this.$('#show-passphrase');

			// https://bugzilla.mozilla.org/show_bug.cgi?id=1405634
			if(checkbox.length < 1) {
				return;
			} else if(checkbox.is(':checked')) {
				phrase.attr('type', 'text');
				setTimeout(function() { phrase.focus(); }, 0);
			} else {
				phrase.attr('type', 'password');
				setTimeout(function() { phrase.focus(); }, 0);
			}
		}
	});

	var PreferencesView = Backbone.View.extend({
		template: createTemplate(jQuery('#preferences_template').html()),
		json_import_export_templates: {
			none: createTemplate(jQuery('#preferences_json_import_export_none_template').html()),
			import: createTemplate(jQuery('#preferences_json_import_template').html()),
			export: createTemplate(jQuery('#preferences_json_export_template').html()),
		},
		remotestorage_sync_templates: {
			none: createTemplate(jQuery('#preferences_remotestorage_sync_none_template').html()),
			import: createTemplate(jQuery('#preferences_remotestorage_sync_import_template').html()),
			export: createTemplate(jQuery('#preferences_remotestorage_sync_export_template').html()),
		},
		erase_data_templates: {
			none: createTemplate(''),
			confirm: createTemplate(jQuery('#preferences_erase_data_confirm_template').html()),
		},
		events: {
			'click .header .back': 'back',
			'click .save-button': 'save',
			'change #enable-lock-time': 'update_locktime',
			'click .json-import-export .import': 'json_show_import',
			'click .json-import-export .export': 'json_show_export',
			'input .json-import-export .json-import .import-data': 'json_update_import_data',
			'click .json-import-export .json-import .do-import': 'json_do_import',
			'click .remotestorage-sync .import': 'remotestorage_show_import',
			'click .remotestorage-sync .export': 'remotestorage_show_export',
			'input .remotestorage-sync .remotestorage-address': 'remotestorage_update_address',
			'click .remotestorage-sync .remotestorage-sync-import .do-import': 'remotestorage_do_import',
			'click .remotestorage-sync .remotestorage-sync-export .do-export': 'remotestorage_do_export',
			'click .erase-data .erase': 'erase_show',
			'click .erase-data .confirm': 'erase_confirm',
			'click .erase-data .cancel': 'erase_cancel',
		},
		initialize: function() {
			this.json_import_export = {state: 'none'};
			this.remotestorage_sync = {state: 'none'};
			this.erase_data = {state: 'none'};
			this.render();
			this.listenTo(prefs, 'all', this.render);
			prefs.fetch();
		},
		render: function() {
			this.$el.html(this.template());
			this.$('.json-import-export .json-import-export-display').html(this.json_import_export_templates[this.json_import_export.state]({
				json_import_export_message: this.json_import_export.message,
				json_import_export_error: this.json_import_export.error,
				json_import_export_data: this.json_import_export.data,
			}));
			this.$('.remotestorage-sync .remotestorage-sync-display').html(this.remotestorage_sync_templates[this.remotestorage_sync.state]({
				remotestorage_sync_message: this.remotestorage_sync.message,
				remotestorage_sync_error: this.remotestorage_sync.error,
				remotestorage_sync_address: this.remotestorage_sync.address,
			}));
			this.$('.erase-data .erase-data-display').html(this.erase_data_templates[this.erase_data.state]());

			if(prefs.get('locktime') > 0) {
				this.$('#enable-lock-time').attr('checked', 'checked');
				this.$('#lock-time').val(prefs.get('locktime'));
			}
			this.update_locktime();

			return this;
		},
		back: function() {
			container.change_view('mainmenu');
		},
		save: function() {
			if(this.$('.save-button').hasClass('disabled')) { return; }
			this.$('.save-button').addClass('disabled');
			var data = {
				locktime: this.$('#enable-lock-time').is(':checked') ? parseInt(this.$('#lock-time').val()) : 0,
			};
			prefs.save(data, {success: function() {
				container.change_view('mainmenu');
			}});
		},
		update_locktime: function() {
			if(this.$('#enable-lock-time').is(':checked')) {
				this.$('.lock-time').removeClass('disabled');
				this.$('#lock-time').removeAttr('disabled');
				if(this.$('#lock-time').val() == '') { this.$('#lock-time').val(5); }
			} else {
				this.$('.lock-time').addClass('disabled');
				this.$('#lock-time').attr('disabled', 'disabled');
			}
		},

		/* JSON Import/Export */

		json_show_export: function() {
			if(this.json_import_export.state === 'export') { return; }
			this.json_import_export = {state: 'export', data: ''};
			this.render();
			runtimeSendMessage({action: 'export'}).then(function(response) {
				this.json_import_export.data = JSON.stringify(response);
				this.render();
			}.bind(this));
		},
		json_show_import: function() {
			if(this.json_import_export.state === 'import') { return; }
			this.json_import_export = {state: 'import', data: ''};
			this.render();
		},
		json_update_import_data: function() {
			if(this.json_import_export.state !== 'import') { return; }
			this.json_import_export.data = this.$('.json-import-export .json-import .import-data').val();
		},
		json_do_import: function() {
			if(this.json_import_export.state !== 'import') { return; }
			this.json_import_export.error = null;
			this.json_import_export.message = null;
			try {
				var importdata = JSON.parse(this.json_import_export.data);
				runtimeSendMessage({action: 'import', data: importdata}).then(function(response) {
					if(response.error) {
						this.json_import_export.error = response.error;
						this.render();
					} else {
						this.json_import_export = {state: 'none', message: 'Data imported'};
						this.render();
						prefs.fetch();
					}
				}.bind(this));
			} catch(e) {
				this.json_import_export.error = e.message;
			}
			this.render();
		},

		/* RemoteStorage Sync */

		remotestorage_show_export: function() {
			if(this.remotestorage_sync.state === 'export') { return; }
			this.remotestorage_sync = {state: 'export', address: prefs.get('remotestorage_address')};
			this.render();
		},
		remotestorage_show_import: function() {
			if(this.remotestorage_sync.state === 'import') { return; }
			this.remotestorage_sync = {state: 'import', address: prefs.get('remotestorage_address')};
			this.render();
		},
		remotestorage_update_address: function() {
			if(this.remotestorage_sync.state !== 'import' && this.remotestorage_sync.state !== 'export') { return; }
			this.remotestorage_sync.address = this.$('.remotestorage-sync .remotestorage-address').val();
		},
		remotestorage_do_import: function() {
			if(this.remotestorage_sync.state !== 'import') { return; }
			this.remotestorage_sync.error = null;
			this.remotestorage_sync.message = null;
			this.render();
			runtimeSendMessage({action: 'remotestorage_import', address: this.remotestorage_sync.address}).then(function(response) {
				if(response.error) {
					this.remotestorage_sync.error = response.error;
					this.render();
				} else {
					this.remotestorage_sync = {state: 'none', message: 'Data imported'};
					this.render();
					prefs.fetch();
				}
			}.bind(this));
		},
		remotestorage_do_export: function() {
			if(this.remotestorage_sync.state !== 'export') { return; }
			this.remotestorage_sync.error = null;
			this.remotestorage_sync.message = null;
			this.render();
			runtimeSendMessage({action: 'remotestorage_export', address: this.remotestorage_sync.address}).then(function(response) {
				if(response.error) {
					this.remotestorage_sync.error = response.error;
					this.render();
				} else {
					this.remotestorage_sync = {state: 'none', message: 'Data exported'};
					this.render();
					prefs.fetch();
				}
			}.bind(this));
		},

		/* Erase Data */

		erase_show: function() {
			if(this.erase_data.state === 'confirm') { return; }
			this.erase_data = {state: 'confirm'};
			this.render();
		},
		erase_confirm: function() {
			if(this.erase_data.state !== 'confirm') { return; }
			this.erase_data = {state: 'none'};
			this.render();
			runtimeSendMessage({action: 'erase_data'}).then(function(response) {
				container.change_view('openvault', {vault_created: false});
			});
		},
		erase_cancel: function() {
			this.erase_data = {state: 'none'};
			this.render();
		},
	});

	var ContainerView = Backbone.View.extend({
		el: $('#container'),
		currentview: null,

		views: {
			'openvault': OpenVaultView,
			'mainmenu': MainMenuView,
			'preferences': PreferencesView,
			'editsite': EditSiteView,
		},

		initialize: function() {
			this.get_vault_state();
		},
		render: function() {
			this.$el.empty().append(this.currentview.render().el);
			return this;
		},
		change_view: function(viewname, options) {
			options = typeof options !== 'undefined' ? options : {};
			if(viewname in this.views) {
				if(this.currentview) { this.currentview.remove(); }
				this.currentview = new this.views[viewname](options);
				this.render();
			}
		},
		get_vault_state: function() {
			var self = this;
			runtimeSendMessage({action: 'get_vault_state'}).then(function(response) {
				if(response.state == 'open') {
					self.change_view('mainmenu');
				} else if(response.state == 'closed') {
					self.change_view('openvault', {vault_created: true});
				} else {
					self.change_view('openvault', {vault_created: false});
				}
			});
		},
	});

	var container = new ContainerView();

	// Utility functions

	function parse_url(url) {
		if(url.search('//') === -1) { url = '//'+url; }
		// url parsing trick from https://gist.github.com/jlong/2428561
		var parser = document.createElement('a');
		parser.href = url;
		return parser;
	}

	function copy_to_clipboard(value) {
		var textarea = document.createElement('textarea');
		textarea.value = value;
		document.body.appendChild(textarea);
		textarea.select();
		document.execCommand('copy');
		document.body.removeChild(textarea);
	}

	// Cross browser functions

	function runtimeOnMessageAddListener(listener) {
		if(window.chrome) {
			return chrome.runtime.onMessage.addListener(listener);
		} else {
			return browser.runtime.onMessage.addListener(listener);
		}
	}

	function runtimeSendMessage(message) {
		if(window.chrome) {
			return new Promise(function(resolve, reject) {
				chrome.runtime.sendMessage(message, function(result) {
					if(chrome.runtime.lastError) {
						reject(chrome.runtime.lastError);
					} else {
						resolve(result);
					}
				});
			});
		} else {
			return browser.runtime.sendMessage(message);
		}
	}

	function tabsSendMessage(tabId, message) {
		if(window.chrome) {
			return new Promise(function(resolve, reject) {
				chrome.tabs.sendMessage(tabId, message, function(result) {
					if(chrome.runtime.lastError) {
						reject(chrome.runtime.lastError);
					} else {
						resolve(result);
					}
				});
			});
		} else {
			return browser.tabs.sendMessage(tabId, message);
		}
	}

	function tabsQuery(options) {
		if(window.chrome) {
			return new Promise(function(resolve, reject) {
				chrome.tabs.query(options, function(result) {
					if(chrome.runtime.lastError) {
						reject(chrome.runtime.lastError);
					} else {
						resolve(result);
					}
				});
			});
		} else {
			return browser.tabs.query(options);
		}
	}

	function tabsCreate(options) {
		if(window.chrome) {
			return new Promise(function(resolve, reject) {
				chrome.tabs.create(options, function(result) {
					if(chrome.runtime.lastError) {
						reject(chrome.runtime.lastError);
					} else {
						resolve(result);
					}
				});
			});
		} else {
			return browser.tabs.create(options);
		}
	}

	function createTemplate(html) {
		return function(data) {
			return Mustache.render(html, data);
		};
	}
});
