# WebVault

WebVault is a simple password manager browser extension designed for people who love the safety and flexibility of the [Vault](https://github.com/jcoglan/vault) password system but dislike interrupting their browsing flow to fire up their console or open [getvau.lt](https://getvau.lt) to grab their passwords. WebVault remembers your service names and password options for each site, and allows you to autofill your passwords after unlocking your vault with your passphrase. This cuts the process of retrieving a password down to clicking the WebVault icon, typing in your passphrase and hitting autofill, all without having to switch pages.

Your passphrase is never stored to disk anywhere at any time, it only lives in sandboxed JavaScript memory for the limited amount of time that you have your WebVault opened. Your service data and preferences are encrypted with AES using your passphrase and saved using Web Storage Sync API so you can have access to them anywhere by logging into your browser (e.g. via Firefox sync, or a Google account). You can also sync your data to a [RemoteStorage](https://remotestorage.io/) account, where it can be stored in an encrypted format that can only be decoded with your passphrase.

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.