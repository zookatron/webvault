jQuery(document).ready(function() {
	var context_menu_set = false;

	jQuery(document).mousedown(function(e) {
		if(e.button !== 2) { return; }

		var el = jQuery(e.target);
		if(typeof el.attr('data-webvault-form-id') !== 'undefined') {
			var form_id = el.attr('data-webvault-form-id').then(function() {}, function() {});
			runtimeSendMessage({action: 'set_context_menu', form_id: form_id});
			context_menu_set = true;
		} else if(context_menu_set) {
			runtimeSendMessage({action: 'reset_context_menu'}).then(function() {}, function() {});
			context_menu_set = false;
		}
	});

	runtimeOnMessageAddListener(function(request, sender, respond) {
		if('action' in request && request.action == 'do_autofill') {
			do_autofill(request.site_id, request.form_id);
		}
	});

	function setValue(element, value) {
		var elementDescriptor = Object.getOwnPropertyDescriptor(element, 'value') || {};
		var valueSetter = elementDescriptor.set;
		var prototype = Object.getPrototypeOf(element);
		var prototypeDescriptor = Object.getOwnPropertyDescriptor(prototype, 'value') || {};
		var prototypeValueSetter = prototypeDescriptor.set;

		if(prototypeValueSetter && valueSetter !== prototypeValueSetter) {
			prototypeValueSetter.call(element, value);
		} else if (valueSetter) {
			valueSetter.call(element, value);
		} else {
			element.value = value;
		}

		var changeEvent = element.ownerDocument.createEvent('Events');
        changeEvent.initEvent('change', !0, !0);
        element.dispatchEvent(changeEvent);

        var inputEvent = element.ownerDocument.createEvent('Events');
        inputEvent.initEvent('input', !0, !0);
        element.dispatchEvent(inputEvent);
	}

	function do_autofill(site_id, form_id) {
		form_id = typeof form_id !== 'undefined' ? form_id : -1;
		runtimeSendMessage({action: 'request_autofill', site_id: site_id, form_id: form_id}).then(function(response) {
			if('error' in response) { return; }
			var forms = get_forms();
			if(forms.length < 1) { return; }
			var form = forms[0];
			for(var i = forms.length - 1; i >= 0; i--) {
				if(forms[i].id === response.form_id) { form = forms[i]; break; }
			}
			if(form.username && form.username.val() == '') { setValue(form.username[0], response.username); }
			if(form.password.val() == '') { setValue(form.password[0], response.password); }
		}, function() {});
	}

	var next_form_id = 0;
	function build_form(form_id, el, filter) {
		var newform = form_template = {
			id: form_id,
			password: null,
			username: null,
		};

		var password = el.find('input[type="password"]').filter(':visible').filter(filter).first();
		if(password.length > 0) {
			var newform = jQuery.extend({}, form_template);
			password.attr('data-webvault-form-id', form_id);
			newform.password = password;
			var username_dist = -1;
			el.find('input:not([type]),input[type="text"],input[type="email"]').filter(':visible').filter(filter).each(function(i, e) {
				var dist = Math.abs(password.offset().top - jQuery(e).offset().top) + Math.abs(password.offset().left - jQuery(e).offset().left)*0.5;
				if(username_dist == -1 || dist < username_dist) {
					username_dist = dist;
					newform.username = jQuery(e);
				}
			});
			if(newform.username) { newform.username.attr('data-webvault-form-id', form_id); }
		}

		return newform;
	}
	function get_forms() {
		var forms = [];

		jQuery('form').each(function(i, e) {
			var el = jQuery(e);
			if(typeof el.attr('data-webvault-form-id') == 'undefined') {
				el.attr('data-webvault-form-id', next_form_id++);
			}
			var newform = build_form(el.attr('data-webvault-form-id'), el, '*');
			if(newform.password) { forms.push(newform); }
		});
		var noneform = build_form('none', jQuery('body'), ':not(form input)');
		if(noneform.password) { forms.push(noneform); }

		return forms;
	}

	function mark_login_fields() {
		var forms = get_forms();
		for (var i = forms.length - 1; i >= 0; i--) {
			var username_image = 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAAAQFJREFUSEvt1M1LAlEUxuHRyj625QcukloIQUELXbiodm5ctVaEXLSISgNRLKX+dH8vzMBl5ggzdxYh+sKzGeaec+fM3Al2IiXc4wULzDHELQ6RKyrexTf+YtTsCUfwThs/iBePqMkdvFLAG6zCLo3OK8f4hVXUpSfUZjLnFPFilhW8GmjRGFZR1yu88wDt0CosGk8L3jnDM6wmS/Sgd5UravKICVRYL/4THZzAKzpg5yg7qmiEKuG1iO5NdeAO0IR+Be/QTtPQvX1coYiNuYFOZ3zeac1wDTOa9QjWwiwG0IgTqeML1qIsPnCBRC4xhbUoC31tNSSybxD5vwbbmCBYA0mNduh1x4mjAAAAAElFTkSuQmCC)';
			var password_image = 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAAq1BMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0NbREAAAAOXRSTlMAAQIDBQcIDA4PERITFBYdICIjKy4vMTM0OTo7PD9ARUdQUVNUVVZdXl9gaXBzdHV2eHl7fH1+f4BI0QaIAAAAsklEQVR42qWP2Q6CMBREKyiiooC7KO4IioqKhf7/lzltYi4Q3jgP7XROc5uyRpirILp4vWqt+VxIuK+X+6P4cyiJuSCWjNATFOluuv9if7ZJuDjnDoKTI7kkFjheVbohrcvipFKAtCExkaM7CMYLaUaiKx+NrNYwxJ71SYwhwEet50KfCiIx6/u7RZ8o9I+tUb3PbTDQaL6NXglWwVN1xt/1Ai9WGXEp6E+EE8ZxbLKm/ABzOydkA7ZdkAAAAABJRU5ErkJggg==)';
			if(forms[i].username) { forms[i].username.css({
				'background-image': username_image,
				'background-position': 'right center',
				'background-size': 'auto 100%',
				'background-repeat': 'no-repeat',
			}); }
			if(forms[i].password) { forms[i].password.css({
				'background-image': password_image,
				'background-position': 'right center',
				'background-size': 'auto 100%',
				'background-repeat': 'no-repeat',
			}); }
		};
	}

	function init() {
		mark_login_fields();

		//setup document mutation observer
		if(typeof MutationObserver != "undefined") {
			var observer = new MutationObserver(function(records) {
				for (var i = records.length - 1; i >= 0; i--) {
					// filter out mutations that don't add potential login inputs
					if(records[i].addedNodes && jQuery(records[i].addedNodes).find('input').length > 0) {
						mark_login_fields();
					}
				}
			});
			observer.observe(document, {childList: true, subtree: true});
		}

		runtimeSendMessage({action: 'query_automatic_fill'}).then(function() {}, function() {});
	}

	init();

	// Cross browser functions

	function runtimeOnMessageAddListener(listener) {
		if(window.chrome) {
			return chrome.runtime.onMessage.addListener(listener);
		} else {
			return browser.runtime.onMessage.addListener(listener);
		}
	}

	function runtimeSendMessage(message) {
		if(window.chrome) {
			return new Promise(function(resolve, reject) {
				chrome.runtime.sendMessage(message, function(result) {
					if(chrome.runtime.lastError) {
						reject(chrome.runtime.lastError);
					} else {
						resolve(result);
					}
				});
			});
		} else {
			return browser.runtime.sendMessage(message);
		}
	}
});
